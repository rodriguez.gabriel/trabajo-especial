FROM node:12.13.1

RUN apt-get update && \
    apt-get -y install curl software-properties-common unzip

### libraries for the ia32/i386 architecture ###
RUN dpkg --add-architecture i386 && \
    apt-get update && \
    apt-get -y install lib32z1 lib32ncurses5 libbz2-1.0:i386 libstdc++6:i386

### G++ compiler ###
RUN apt-get -y install g++

### JDK 8 ###
RUN apt-get -y install openjdk-8-jdk && \
    update-alternatives --config java
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64

### Re-add all the CA certs into the previously empty file ###
RUN /var/lib/dpkg/info/ca-certificates-java.postinst configure

### Android SDK ###
WORKDIR /usr/local/android/sdk
RUN curl -sL https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip > sdk-tools-linux.zip && \
    unzip -q sdk-tools-linux.zip && rm sdk-tools-linux.zip
ENV ANDROID_HOME /usr/local/android/sdk/
ENV PATH ${PATH}:${ANDROID_HOME}tools/:${ANDROID_HOME}platform-tools/

RUN echo "y" | tools/bin/sdkmanager "tools" "emulator" "platform-tools" "platforms;android-28" "build-tools;28.0.3" "extras;android;m2repository" "extras;google;m2repository"

### Install NativeScript ###
RUN npm install -g nativescript

WORKDIR /app