1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="org.nativescript.TrabajoEspecial"
4    android:versionCode="10000"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="17"
8-->/app/platforms/android/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="28" />
9-->/app/platforms/android/app/src/main/AndroidManifest.xml
10
11    <supports-screens
11-->/app/platforms/android/app/src/main/AndroidManifest.xml:7:2-11:33
12        android:largeScreens="true"
12-->/app/platforms/android/app/src/main/AndroidManifest.xml:10:3-30
13        android:normalScreens="true"
13-->/app/platforms/android/app/src/main/AndroidManifest.xml:9:3-31
14        android:smallScreens="true"
14-->/app/platforms/android/app/src/main/AndroidManifest.xml:8:3-30
15        android:xlargeScreens="true" />
15-->/app/platforms/android/app/src/main/AndroidManifest.xml:11:3-31
16
17    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
17-->/app/platforms/android/app/src/main/AndroidManifest.xml:13:2-76
17-->/app/platforms/android/app/src/main/AndroidManifest.xml:13:19-73
18    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
18-->/app/platforms/android/app/src/main/AndroidManifest.xml:14:2-78
18-->/app/platforms/android/app/src/main/AndroidManifest.xml:14:19-75
19    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
19-->/app/platforms/android/app/src/main/AndroidManifest.xml:15:2-76
19-->/app/platforms/android/app/src/main/AndroidManifest.xml:15:19-74
20    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
20-->/app/platforms/android/app/src/main/AndroidManifest.xml:16:2-77
20-->/app/platforms/android/app/src/main/AndroidManifest.xml:16:19-75
21    <uses-permission android:name="android.permission.INTERNET" />
21-->/app/platforms/android/app/src/main/AndroidManifest.xml:17:2-63
21-->/app/platforms/android/app/src/main/AndroidManifest.xml:17:19-61
22
23    <application
23-->/app/platforms/android/app/src/main/AndroidManifest.xml:19:2-40:16
24        android:name="com.tns.NativeScriptApplication"
24-->/app/platforms/android/app/src/main/AndroidManifest.xml:20:3-49
25        android:allowBackup="true"
25-->/app/platforms/android/app/src/main/AndroidManifest.xml:21:3-29
26        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
26-->[androidx.core:core:1.0.1] /root/.gradle/caches/transforms-2/files-2.1/3d3d0b77ddd3e70ec0512eb77638feaf/core-1.0.1/AndroidManifest.xml:22:18-86
27        android:debuggable="true"
28        android:icon="@drawable/icon"
28-->/app/platforms/android/app/src/main/AndroidManifest.xml:22:3-32
29        android:label="@string/app_name"
29-->/app/platforms/android/app/src/main/AndroidManifest.xml:23:3-35
30        android:theme="@style/AppTheme" >
30-->/app/platforms/android/app/src/main/AndroidManifest.xml:24:3-34
31        <activity
31-->/app/platforms/android/app/src/main/AndroidManifest.xml:26:3-38:14
32            android:name="com.tns.NativeScriptActivity"
32-->/app/platforms/android/app/src/main/AndroidManifest.xml:27:4-47
33            android:configChanges="keyboard|keyboardHidden|orientation|screenSize|smallestScreenSize|screenLayout|locale|uiMode"
33-->/app/platforms/android/app/src/main/AndroidManifest.xml:29:4-120
34            android:label="@string/title_activity_kimera"
34-->/app/platforms/android/app/src/main/AndroidManifest.xml:28:4-49
35            android:theme="@style/LaunchScreenTheme" >
35-->/app/platforms/android/app/src/main/AndroidManifest.xml:30:4-44
36            <meta-data
36-->/app/platforms/android/app/src/main/AndroidManifest.xml:32:4-87
37                android:name="SET_THEME_ON_LAUNCH"
37-->/app/platforms/android/app/src/main/AndroidManifest.xml:32:15-49
38                android:resource="@style/AppTheme" />
38-->/app/platforms/android/app/src/main/AndroidManifest.xml:32:50-84
39
40            <intent-filter>
40-->/app/platforms/android/app/src/main/AndroidManifest.xml:34:4-37:20
41                <action android:name="android.intent.action.MAIN" />
41-->/app/platforms/android/app/src/main/AndroidManifest.xml:35:5-57
41-->/app/platforms/android/app/src/main/AndroidManifest.xml:35:13-54
42
43                <category android:name="android.intent.category.LAUNCHER" />
43-->/app/platforms/android/app/src/main/AndroidManifest.xml:36:5-65
43-->/app/platforms/android/app/src/main/AndroidManifest.xml:36:15-62
44            </intent-filter>
45        </activity>
46        <activity android:name="com.tns.ErrorReportActivity" />
46-->/app/platforms/android/app/src/main/AndroidManifest.xml:39:3-57
46-->/app/platforms/android/app/src/main/AndroidManifest.xml:39:13-55
47        <activity
47-->[com.google.android.gms:play-services-base:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/ec5fff3415317cdd1578a2e823585501/jetified-play-services-base-11.4.0/AndroidManifest.xml:19:9-172
48            android:name="com.google.android.gms.common.api.GoogleApiActivity"
48-->[com.google.android.gms:play-services-base:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/ec5fff3415317cdd1578a2e823585501/jetified-play-services-base-11.4.0/AndroidManifest.xml:19:19-85
49            android:exported="false"
49-->[com.google.android.gms:play-services-base:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/ec5fff3415317cdd1578a2e823585501/jetified-play-services-base-11.4.0/AndroidManifest.xml:19:146-170
50            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
50-->[com.google.android.gms:play-services-base:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/ec5fff3415317cdd1578a2e823585501/jetified-play-services-base-11.4.0/AndroidManifest.xml:19:86-145
51
52        <meta-data
52-->[com.google.android.gms:play-services-basement:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/e2ac3d9398780f382b4e6541709de8fb/jetified-play-services-basement-11.4.0/AndroidManifest.xml:20:9-121
53            android:name="com.google.android.gms.version"
53-->[com.google.android.gms:play-services-basement:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/e2ac3d9398780f382b4e6541709de8fb/jetified-play-services-basement-11.4.0/AndroidManifest.xml:20:20-65
54            android:value="@integer/google_play_services_version" />
54-->[com.google.android.gms:play-services-basement:11.4.0] /root/.gradle/caches/transforms-2/files-2.1/e2ac3d9398780f382b4e6541709de8fb/jetified-play-services-basement-11.4.0/AndroidManifest.xml:20:66-119
55    </application>
56
57</manifest>
