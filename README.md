# TrabajoEspecial

Trabajo integrador de la cursada Enfoques de Desarrollo de Aplicaciones Móviles Multiplataforma.

## Requerimientos

* NativeScript ~6.2.2
* Node ~12.13.1
* Java JDK ~8
* Android SDK ~26.1.1
* Android SDK Build-tools ~28.0.3
* Android Support Repository
* G++ compiler

## Instalación

Se recomienda utilizar docker.

### Docker

Construimos la imagen:
```
docker-compose build
```

Iniciamos el contenedor:
```
docker-compose up
```

### Manual

Completar los siguientes pasos para configurar NativeScript en su maquina local

https://docs.nativescript.org/start/ns-setup-linux#advanced-setup-steps

## Generar APK

Ejecutar el siguiente comando:
```
tns build android
```

En caso de usar _docker_:
```
docker-compose run web tns build android
```

El resultado de la compilación se encuentra en _/app/platforms/android/app/build/outputs/apk/debug/app-debug.apk_.

## Nota

En _docs/desarrollo.txt_ se encuentra información acerca de la toma de decisiones y documentación de la API utilizada.